# Welcome

## Get Started
To replicate all experiments and results run the code in this repo.
Let's install dependencies, handled by poetry, if you don't have poetry installed, install it first with `curl -sSL https://install.python-poetry.org | python3 -`, then proceed:
```
git clone https://gitlab.com/barryboy/jbc090-project.git
cd jbc090-project
poetry install
```
now to activate the virtualenvironment every time you want to open the project do:
```
cd <path/to>/jbc090-project
poetry shell
```
## Replicate Experiments
```
python3 langproj/main.py
python3 langproj/nlp.py
```
- *main*: contains data processing
- *nlp*: searches & trains best classifier

### Troubleshooting
