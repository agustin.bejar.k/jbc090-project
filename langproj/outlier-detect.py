from sklearn.svm import OneClassSVM
from sklearn.metrics import mean_absolute_error
import numpy as np
import pandas as pd

data = pd.read_csv("./cache/res.csv")

def outliers_per_comunity(data, community):
    data_community = data[ data["pred_class"].str.contains("__label__%s" % community) ]
    X = np.vstack(data_community["embedding"].apply(lambda x: np.frombuffer(eval(x), dtype=np.float32)))
    
    ee = OneClassSVM(nu=0.001)
    outlier_mask = ee.fit_predict(X) == -1
    outliers = data_community[ outlier_mask ]
    return outliers


store = {community: outliers_per_comunity(data, community)
         for community in ["conservative", "liberal", "democrat", "republican"]}

import pickle

with open('./cache/outliers.pkl', 'wb') as f:
    pickle.dump(store, f)

# Load
with open('./cache/outliers.pkl', 'rb') as f:
    loaded_store = pickle.load(f)
