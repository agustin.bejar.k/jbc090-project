import pandas as pd
from pprint import pprint as pp
import wget
import matplotlib.pyplot as plt
from gensim.utils import simple_preprocess

# download data
data_url_5k = "https://surfdrive.surf.nl/files/index.php/s/9ROTj6HWRAlvngn/download"
data_url_10k = "https://surfdrive.surf.nl/files/index.php/s/9ROTj6HWRAlvngn/download"
data_url_50k = "https://surfdrive.surf.nl/files/index.php/s/jBTUfgZQ5lXmZn9/download"

filename = wget.download(data_url_50k)
df = pd.read_csv(filename, na_values=["[deleted]", "[removed]"], nrows=50000).dropna()
print(f"\n downloaded raw data with {len(df)} entries, preprocessing...")


# data processing
df = df.drop_duplicates(subset=["text"])

# balance classes
print("before class balancing:")
df_len = len(df)
data_before = df["subreddit"].value_counts().to_dict()
pp(data_before)

conservative_rows = df[df['subreddit'] == 'Conservative']
sampled_conservative_rows = conservative_rows.sample(frac=0.1)
other_labels_rows = df[df['subreddit'] != 'Conservative']
df = pd.concat([sampled_conservative_rows, other_labels_rows])

print("after class balancing:")
data_after = df["subreddit"].value_counts().to_dict()
pp(data_after)

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
fig.suptitle("Class Frequencies Before and After Balancing")
axes[0].bar(data_before.keys(), [x / df_len for x in data_before.values()], color='blue')
axes[0].set_title("Before Class Balancing")
axes[0].set_xlabel("Class")
axes[0].set_ylabel("% Frequency")
axes[1].bar(data_after.keys(), [x / df_len for x in data_after.values()], color='orange')
axes[1].set_title("After Class Balancing")
axes[1].set_xlabel("Class")
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig("classes_balancing.svg")
#plt.show()

# some simple NLP preprocessing
df["text"] = df["text"].apply(lambda x: " ".join(simple_preprocess(x)))
df["fasttext"] = "__label__" + df["subreddit"] + " " + df["text"]
df["fasttext"] = df["fasttext"].str.lower()
df.dropna(inplace=True)
print(df.info())

# train / val / test split
df_shuffled = df.sample(frac=1).reset_index(drop=True)
train_size = int(0.8 * len(df_shuffled))
val_size = int(0.1 * len(df_shuffled))
print(f"train size: {train_size}, test size: {len(df_shuffled) - train_size}")
train_df = df_shuffled[:train_size]
val_df = df_shuffled[train_size:train_size+val_size]
test_df = df_shuffled[train_size+val_size:]

with open('train_data.txt', 'w') as file:
    for entry in train_df['fasttext']:
        file.write(entry + '\n')

with open('val_data.txt', 'w') as file:
    for entry in train_df['fasttext']:
        file.write(entry + '\n')

with open('test_data.txt', 'w') as file:
    for entry in test_df['fasttext']:
        file.write(entry + '\n')


print(df_shuffled.info(), df_shuffled.head())
df_shuffled.to_csv("df_shuffled.csv", index=False)
