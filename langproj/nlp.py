import fasttext
from pprint import pprint as pp


model = fasttext.train_supervised(
    "train_data.txt",
   #lr=0.01,
    ws=10,
   #dim=100,
   #epoch=500,
   #wordNgrams=4,
    thread=8,
    autotuneValidationFile='val_data.txt',
    autotuneDuration=600 * 2,
    autotuneModelSize="800M")

model.save_model('model_searched.bin')
model = fasttext.load_model("model_searched.bin")

print("best model on eval :%s" % model.test("test_data.txt"))
print("best model on test :%s" % model.test("test_data.txt"))


# eval model
pp(model.test_label("test_data.txt"))

print("\n------------\nSanity Check")
#print(model.predict('i hate leftists', k=2))
#print(model.predict('i hate right wing ', k=2))

print(model.get_sentence_vector("i am an immigrant").shape)
