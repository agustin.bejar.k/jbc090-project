import pandas as pd
import fasttext

model = fasttext.load_model("model_searched.bin")

df = pd.read_csv("df_shuffled.csv")
df["embedding"] = [model.get_sentence_vector(x).tobytes() for x in df["fasttext"]]
df["pred_class"] = [model.predict(x) for x in df["fasttext"]]
print(df.info())
df.to_csv("res.csv")
